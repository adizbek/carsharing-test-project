import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { EstimateBookingDto } from './dto/estimate-booking.dto';
import { BookingService } from './booking.service';
import { EstimateBookingResultDto } from './dto/estimate-booking-result.dto';
import { BookDto } from './dto/book.dto';
import BookingEntity from './entity/booking.entity';

@Controller('booking')
export class BookingController {
  constructor(private bookingService: BookingService) {}

  @Get('/estimate')
  estimateBooking(
    @Query() { tariff: tariffId, from, to }: EstimateBookingDto,
  ): EstimateBookingResultDto {
    const days = this.bookingService.estimateBookingDays(from, to);
    const tariff = this.bookingService.getTariff(tariffId);

    return {
      price: this.bookingService.bookingPrice(tariff, days),
      priceWithDiscount: this.bookingService.bookingPriceWithDiscount(
        tariff,
        days,
      ),
    };
  }

  @Post('/book')
  async book(@Body() data: BookDto): Promise<BookingEntity> {
    return await this.bookingService.book(
      data.car,
      data.tariff,
      data.from,
      data.to,
    );
  }
}
