import { Module } from '@nestjs/common';
import { BookingController } from './booking.controller';
import { BookingService } from './booking.service';
import { IsValidTariffRule } from './validations/IsValidTariff';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  exports: [BookingService],
  controllers: [BookingController],
  providers: [BookingService, IsValidTariffRule],
})
export class BookingModule {}
