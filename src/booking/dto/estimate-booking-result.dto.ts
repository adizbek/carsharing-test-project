export interface EstimateBookingResultDto {
  price: number;
  priceWithDiscount: number;
}
