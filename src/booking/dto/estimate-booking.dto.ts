import { IsDate, IsInt } from 'class-validator';
import { IsValidTariff } from '../validations/IsValidTariff';
import { Type } from 'class-transformer';

export class EstimateBookingDto {
  @IsInt()
  @IsValidTariff()
  @Type(() => Number)
  tariff: number;

  @IsDate()
  @Type(() => Date)
  from: Date;

  @IsDate()
  @Type(() => Date)
  to: Date;
}
