import { EstimateBookingDto } from './estimate-booking.dto';
import { IsInt } from 'class-validator';
import { Type } from 'class-transformer';

export class BookDto extends EstimateBookingDto {
  @IsInt()
  @Type(() => Number)
  car: number;
}
