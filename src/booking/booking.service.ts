import { Injectable } from '@nestjs/common';
import { Tariff } from './interfaces/tariff.interface';
import { DateTime } from 'luxon';
import { InjectKnex, Knex } from 'nestjs-knex';
import BookingEntity from './entity/booking.entity';
import NoCarsAvailableError from './exceptions/NoCarsAvailableError';
import InvalidBookingPeriodError from './exceptions/InvalidBookingPeriodError';
import { QueryResult } from 'pg';
import { IsCarAvailableInterface } from './interfaces/is-car-available.interface';

@Injectable()
export class BookingService {
  constructor(@InjectKnex() private readonly knex: Knex) {}

  getTariff(id: number): Tariff {
    return this.tariffs().find((t) => t.id === id);
  }

  estimateBookingDays(from: Date, to: Date): number {
    const toDt = DateTime.fromJSDate(to).startOf('day');
    const fromDt = DateTime.fromJSDate(from).startOf('day');

    const days = toDt.diff(fromDt).as('days') + 1;

    if (fromDt < DateTime.now().startOf('day') || days < 0) {
      throw new InvalidBookingPeriodError(
        'Start date should not be in past and toDate must be equal or greater than fromDate',
      );
    }

    if (toDt.get('weekday') >= 6 || fromDt.get('weekday') >= 6) {
      throw new InvalidBookingPeriodError('Start or ending can not be weekend');
    }

    return days;
  }

  tariffs(): Tariff[] {
    return [
      { id: 1, price: 270, distance: 200 },
      { id: 2, price: 330, distance: 350 },
      { id: 3, price: 390, distance: 500 },
    ];
  }

  bookingPrice(tariff: Tariff, bookingDays: number): number {
    return tariff.price * bookingDays;
  }

  bookingPriceWithDiscount(tariff: Tariff, bookingDays): number {
    if (bookingDays < 3) {
      return this.bookingPrice(tariff, bookingDays);
    } else if (bookingDays >= 3 && bookingDays <= 5) {
      return this.bookingPrice(tariff, bookingDays) * 0.95;
    } else if (bookingDays >= 6 && bookingDays <= 14) {
      return this.bookingPrice(tariff, bookingDays) * 0.9;
    } else if (bookingDays >= 15 && bookingDays <= 30) {
      return this.bookingPrice(tariff, bookingDays) * 0.85;
    } else
      throw new InvalidBookingPeriodError('Can not book more than 30 days');
  }

  async book(
    carId: number,
    tariffId: number,
    from: Date,
    to: Date,
  ): Promise<BookingEntity> {
    const days = this.estimateBookingDays(from, to);

    const exists = await this.isCarAvailableDuringPeriod(carId, from, to);

    if (!exists) {
      throw new NoCarsAvailableError('During the period no cars available');
    }

    const tariff = this.getTariff(tariffId);
    const price = this.bookingPriceWithDiscount(tariff, days);

    const booking = await this.knex.table<BookingEntity>('bookings').insert(
      {
        car_id: carId,
        tariff: tariffId,
        price,
        book_from: from,
        book_to: to,
      },
      '*',
    );

    return booking[0];
  }

  async isCarAvailableDuringPeriod(
    carId: number,
    from: Date,
    to: Date,
  ): Promise<boolean> {
    const result = await this.knex.raw<QueryResult<IsCarAvailableInterface>>(
      `select (select count(id)::int from cars where id = :carId)
           as carExists,
       (select count(id)::int from bookings where car_id = :carId and
       (book_from >= date_trunc('day', :startDate::timestamp) - interval '3' day and
        book_to <= date_trunc('day', :endDate::timestamp) + interval '3'))
           as booked`,
      {
        carId: carId,
        startDate: from,
        endDate: to,
      },
    );

    const booking = result.rows[0];

    return booking.carExists > 0 && booking.booked === 0;
  }
}
