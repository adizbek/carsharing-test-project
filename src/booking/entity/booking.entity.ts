export default class BookingEntity {
  id: number;
  price: number;
  car_id: number;
  tariff: number;
  book_from: Date;
  book_to: Date;

  constructor(partial: Partial<BookingEntity>) {
    Object.assign(this, partial);
  }
}
