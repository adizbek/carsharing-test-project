import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { BookingService } from '../booking.service';

@ValidatorConstraint({ name: 'IsValidTariff', async: false })
@Injectable()
export class IsValidTariffRule implements ValidatorConstraintInterface {
  constructor(private bookingService: BookingService) {}

  validate(tariff: number, args: ValidationArguments) {
    return this.bookingService.getTariff(tariff) !== null;
  }

  defaultMessage(args: ValidationArguments) {
    return 'Tariff ($value) not found!';
  }
}

export function IsValidTariff(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsValidTariff',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsValidTariffRule,
    });
  };
}
