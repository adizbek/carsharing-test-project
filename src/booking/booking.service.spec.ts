import { Test, TestingModule } from '@nestjs/testing';
import { BookingService } from './booking.service';
import InvalidBookingPeriodError from './exceptions/InvalidBookingPeriodError';
import { getKnexConnectionToken, Knex } from 'nestjs-knex';
import { QueryResult } from 'pg';
import BookingEntity from './entity/booking.entity';
import NoCarsAvailableError from './exceptions/NoCarsAvailableError';
import { IsCarAvailableInterface } from './interfaces/is-car-available.interface';

describe('BookingService', () => {
  let service: BookingService;
  let knex: Knex;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingService,
        {
          provide: getKnexConnectionToken(undefined),
          useValue: {
            raw: jest.fn(),
            insert: jest.fn(),
            table: jest.fn().mockReturnThis(),
          },
        },
      ],
    }).compile();

    service = module.get<BookingService>(BookingService);
    knex = module.get<Knex>(getKnexConnectionToken(undefined));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should find correct tariff by id', function () {
    expect(service.getTariff(1)).toEqual(service.tariffs()[0]);
  });

  it('should calculate price correctly', function () {
    const tariff = service.getTariff(1);

    expect(service.bookingPrice(tariff, 10)).toEqual(tariff.price * 10);
  });

  it('should calculate discount price correctly', function () {
    const tariff = service.getTariff(1);

    const price0 = service.bookingPriceWithDiscount(tariff, 2);
    const price1 = service.bookingPriceWithDiscount(tariff, 5);
    const price2 = service.bookingPriceWithDiscount(tariff, 12);
    const price3 = service.bookingPriceWithDiscount(tariff, 30);

    expect(price0).toEqual(tariff.price * 2);
    expect(price1).toEqual(tariff.price * 5 * 0.95);
    expect(price2).toEqual(tariff.price * 12 * 0.9);
    expect(price3).toEqual(tariff.price * 30 * 0.85);

    expect(() => {
      service.bookingPriceWithDiscount(tariff, 50);
    }).toThrow(InvalidBookingPeriodError);
  });

  it('should estimate booking days correctly', function () {
    const day1 = service.estimateBookingDays(
      new Date(2060, 0, 1), //thursday, january
      new Date(2060, 0, 2),
    );

    expect(day1).toBe(2);

    const day2 = service.estimateBookingDays(
      new Date(2060, 0, 1), //thursday, january
      new Date(2060, 0, 1),
    );

    expect(day2).toBe(1);
  });

  it('should be invalid if date in past', function () {
    expect(() =>
      service.estimateBookingDays(
        new Date(2021, 9, 9), //saturday, october, in past
        new Date(2021, 9, 9),
      ),
    ).toThrow(InvalidBookingPeriodError);
  });

  it('should be invalid if end before start', function () {
    expect(() =>
      service.estimateBookingDays(new Date(2060, 9, 10), new Date(2059, 9, 10)),
    ).toThrow(InvalidBookingPeriodError);
  });

  it('should be invalid if start or end of booking day is weekend', function () {
    // saturday
    expect(() =>
      service.estimateBookingDays(new Date(2060, 0, 3), new Date(2060, 9, 3)),
    ).toThrow(InvalidBookingPeriodError);

    // sunday
    expect(() =>
      service.estimateBookingDays(new Date(2060, 0, 4), new Date(2060, 0, 4)),
    ).toThrow(InvalidBookingPeriodError);
  });

  it('should return false if isCarAvailableDuringPeriod returns booked greater than 0', async function () {
    const result: Partial<QueryResult<IsCarAvailableInterface>> = {
      rows: [{ booked: 1, carExists: 1 }],
    };

    jest.spyOn(knex, 'raw').mockResolvedValueOnce(result);

    const isAvailable = await service.isCarAvailableDuringPeriod(
      1,
      new Date(2020, 0, 1),
      new Date(2020, 0, 1),
    );

    expect(isAvailable).toBeFalsy();
  });

  it('should return true if isCarAvailableDuringPeriod returns booked eq 0', async function () {
    const result: Partial<QueryResult<IsCarAvailableInterface>> = {
      rows: [{ booked: 0, carExists: 1 }],
    };

    jest.spyOn(knex, 'raw').mockResolvedValueOnce(result);

    const isAvailableTrue = await service.isCarAvailableDuringPeriod(
      1,
      new Date(2020, 0, 1),
      new Date(2020, 0, 1),
    );

    expect(isAvailableTrue).toBeTruthy();
  });

  it('should return false if there is no car with such id', async function () {
    const result: Partial<QueryResult<IsCarAvailableInterface>> = {
      rows: [{ booked: 0, carExists: 0 }],
    };

    jest.spyOn(knex, 'raw').mockResolvedValueOnce(result);

    const isAvailableTrue = await service.isCarAvailableDuringPeriod(
      1,
      new Date(2020, 0, 1),
      new Date(2020, 0, 1),
    );

    expect(isAvailableTrue).toBeFalsy();
  });

  it('should book car successfully ', async function () {
    jest
      .spyOn(service, 'isCarAvailableDuringPeriod')
      .mockResolvedValueOnce(true);

    const start = new Date(2060, 0, 1);
    const end = new Date(2060, 0, 1);

    const price = service.bookingPriceWithDiscount(service.getTariff(1), 1);

    const insertResult: BookingEntity[] = [
      {
        id: 1000,
        car_id: 1,
        tariff: 1,
        price: price,
        book_from: start,
        book_to: end,
      },
    ];

    jest.spyOn(knex, 'insert').mockResolvedValueOnce(insertResult);

    const booking = await service.book(1, 1, start, end);

    expect(booking).toMatchObject(insertResult[0]);
  });

  it('should throw if no cars available to book', async function () {
    jest
      .spyOn(service, 'isCarAvailableDuringPeriod')
      .mockResolvedValueOnce(false);

    const start = new Date(2060, 0, 1);
    const end = new Date(2060, 0, 1);

    expect.assertions(1);

    await expect(service.book(1, 1, start, end)).rejects.toBeInstanceOf(
      NoCarsAvailableError,
    );
  });
});
