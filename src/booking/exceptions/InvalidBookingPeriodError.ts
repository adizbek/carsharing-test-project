import { BadRequestException } from '@nestjs/common';

export default class InvalidBookingPeriodError extends BadRequestException {
  constructor(message: string) {
    super({ message, status: 400 });
  }
}
