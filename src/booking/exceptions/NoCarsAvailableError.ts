import { BadRequestException } from '@nestjs/common';

export default class NoCarsAvailableError extends BadRequestException {
  constructor(message: string) {
    super({ message, status: 400 });
  }
}
