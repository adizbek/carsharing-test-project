import { Test, TestingModule } from '@nestjs/testing';
import { BookingController } from './booking.controller';
import { BookingService } from './booking.service';
import { EstimateBookingResultDto } from './dto/estimate-booking-result.dto';
import BookingEntity from './entity/booking.entity';

describe('BookingController', () => {
  let controller: BookingController;
  let bookingService: BookingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookingController],
      providers: [
        {
          provide: BookingService,
          useValue: {
            estimateBookingDays: jest.fn(),
            getTariff: jest.fn(),
            bookingPrice: jest.fn(),
            bookingPriceWithDiscount: jest.fn(),
            book: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<BookingController>(BookingController);
    bookingService = module.get<BookingService>(BookingService);
  });

  it('should estimate booking price', () => {
    jest.spyOn(bookingService, 'bookingPrice').mockReturnValueOnce(1000);
    jest
      .spyOn(bookingService, 'bookingPriceWithDiscount')
      .mockReturnValueOnce(900);

    const result = controller.estimateBooking({
      tariff: 1,
      from: new Date(2020, 0, 1),
      to: new Date(2020, 0, 1),
    });

    expect(result).toMatchObject({
      price: 1000,
      priceWithDiscount: 900,
    } as EstimateBookingResultDto);
  });

  it('should book successfully', async function () {
    const date = new Date(2020, 0, 1);

    const insertedBooking = {
      id: 1000,
      car_id: 1,
      tariff: 1,
      price: 1000,
      book_to: date,
      book_from: date,
    } as BookingEntity;

    jest.spyOn(bookingService, 'book').mockResolvedValueOnce(insertedBooking);

    const result = await controller.book({
      car: 1,
      tariff: 1,
      from: date,
      to: date,
    });

    expect(result).toMatchObject(insertedBooking);
  });
});
