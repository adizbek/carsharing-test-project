export interface Tariff {
  id: number;
  price: number;
  distance: number;
}
