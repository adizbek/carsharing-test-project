import { Global, Module } from '@nestjs/common';
import { KnexModule } from 'nestjs-knex';

@Global()
@Module({
  imports: [
    KnexModule.forRootAsync({
      useFactory: () => ({
        config: {
          client: 'pg',
          connection: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            database: process.env.DB_NAME,
          },
        },
      }),
    }),
  ],
})
export class DatabaseModule {}
