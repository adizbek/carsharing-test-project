FROM node:16.11.1-alpine3.14

WORKDIR app

COPY package.json yarn.lock ./

RUN yarn && yarn cache clean

COPY . .

RUN yarn build

CMD yarn start:prod
