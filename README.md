## Carsharing app


## Installation

```bash
$ yarn install
```

Copy `.env.template` to `.env`, if you run `docker-compose` you don't need to change anything.

To start docker services `docker-compose up -d`

To stop docker services `docker-compose stop`


Run migrations `knex migrate:latest` and seed database with fake data `knex seed:run`

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
