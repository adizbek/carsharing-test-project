import { Knex } from 'knex';
import * as faker from 'faker';

export async function seed(knex: Knex): Promise<void> {
  await knex('bookings').del();
  await knex('cars').del();

  await cars(knex);
  await bookings(knex);
}

async function cars(knex: Knex) {
  await knex('cars').insert([
    {
      id: 1,
      brand: 'Chevrolet',
      model: 'Nexia',
      gov_num: '01X555XA',
      vin: '4Y1SL65848Z411439',
    },
    {
      id: 2,
      brand: 'Chevrolet',
      model: 'Lacetti',
      gov_num: '01X355XA',
      vin: '4Y1SL65848Z411435',
    },
    {
      id: 3,
      brand: 'Chevrolet',
      model: 'Malibu 2',
      gov_num: '01X525XA',
      vin: '4Y1SL65848Z411425',
    },
    {
      id: 4,
      brand: 'Chevrolet',
      model: 'Cobalt',
      gov_num: '01X656XA',
      vin: '5Y1SL65848Z411435',
    },
    {
      id: 5,
      brand: 'Chevrolet',
      model: 'Tahoe',
      gov_num: '01Z777XA',
      vin: '1Y1SL65848Z411435',
    },
  ]);
}

async function bookings(knex: Knex) {
  const bookings = [];

  for (let i = 0; i < 50; i++) {
    const carId = faker.datatype.number({ min: 1, max: 5 });
    const tariff = faker.datatype.number({ min: 1, max: 3 });

    const fromDate = faker.datatype.datetime({
      min: new Date(2021, 1, 1).getTime(),
      max: new Date(2021, 10, 1).getTime(),
    });

    const toDate = new Date(
      faker.datatype.number({ min: 3, max: 30 }) + (fromDate.getTime() + 86400),
    );

    bookings.push({
      car_id: carId,
      tariff: tariff,
      book_from: fromDate,
      book_to: toDate,
      price: faker.datatype.number({ min: 100, max: 5000 }),
    });
  }

  await knex('bookings').insert(bookings);
}
