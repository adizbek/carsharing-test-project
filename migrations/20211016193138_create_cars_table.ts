import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('cars', (table) => {
    table.bigIncrements('id');

    table.string('brand', 50);
    table.string('model', 50);
    table.string('gov_num', 50);
    table.string('vin', 50);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTableIfExists('cars');
}
