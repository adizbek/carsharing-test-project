import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('bookings', (table) => {
    table.bigIncrements('id');

    table.bigInteger('car_id').unsigned().notNullable();

    table.integer('tariff').unsigned().notNullable();

    table.dateTime('book_from').notNullable();
    table.dateTime('book_to').notNullable();

    table.decimal('price').unsigned().notNullable();

    table.foreign('car_id').references('cars.id').onDelete('CASCADE');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTableIfExists('bookings');
}
